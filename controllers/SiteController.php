<?php

namespace app\controllers;

use BenMajor\ExchangeRatesAPI\ExchangeRatesAPI;
use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\forms\CurrencyForm;

class SiteController extends Controller
{
    public $countryWithFlags = [
        "CAD" => "CAD",
        "HKD" => "HKD",
        "ISK" => "ISK",
        "PHP" => "PHP",
        "DKK" => "DKK",
        "HUF" => "HUF",
        "CZK" => "CZK",
        "AUD" => "AUD",
        "RON" => "RON",
        "SEK" => "SEK",
        "IDR" => "IDR",
        "INR" => "INR",
        "BRL" => "BRL",
        "RUB" => "RUB",
        "HRK" => "HRK",
        "JPY" => "JPY",
        "THB" => "THB",
        "CHF" => "CHF",
        "SGD" => "SGD",
        "PLN" => "PLN",
        "BGN" => "BGN",
        "TRY" => "TRY",
        "CNY" => "CNY",
        "NOK" => "NOK",
        "NZD" => "NZD",
        "ZAR" => "ZAR",
        "USD" => "USD",
        "MXN" => "MXN",
        "ILS" => "ILS",
        "GBP" => "GBP",
        "KRW" => "KRW",
        "MYR" => "MYR",
        "EUR" => "EUR",
    ];

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionPackage()
    {
        $model = new CurrencyForm();
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if ($model->from && $model->to && $model->amount) {
                $api = new ExchangeRatesAPI();
                $model->ratedAmount = $api->setBaseCurrency($model->from)->convert($model->to, $model->amount);
                $rates = $api->fetch()->getRates();
                $model->rate = $rates[$model->to];
            }
        }
        return $this->render('package', [
            'countries' => $this->countryWithFlags,
            'model' => $model
        ]);
    }

    public function actionJquery()
    {
        $this->enableCsrfValidation = false;
        if (Yii::$app->request->isPost && Yii::$app->request->post('from') && Yii::$app->request->post('to') && Yii::$app->request->post('amount')) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $api = new ExchangeRatesAPI();
            $ratedAmount = $api->setBaseCurrency(Yii::$app->request->post('from'))->convert(Yii::$app->request->post('to'), Yii::$app->request->post('amount'));
            $rates = $api->fetch()->getRates();
            return [
                'rate' => $rates[Yii::$app->request->post('to')],
                'ratedAmount' => $ratedAmount
            ];
        }
        return $this->render('jquery', [
            'countries' => $this->countryWithFlags,
        ]);
    }
    public function actionJs()
    {
        return $this->render('js', [
            'countries' => $this->countryWithFlags,
        ]);
    }
}
