<?php

namespace app\forms;

use Yii;
use yii\base\Model;

class CurrencyForm extends Model
{
    public $from;
    public $to;
    public $amount;
    public $rate;
    public $ratedAmount;

    public function rules()
    {
        return [
            [['from', 'to', 'amount', 'rate', 'ratedAmount'], 'safe']
        ];
    }
}
