<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */


use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = 'With Package';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('/js/js.js', ['depends' => 'yii\web\JqueryAsset']);
?>

<div class="row g-3 bg-light">
    <h5>There is no `benmajor/exchange-rates-api` package and it calls endpoints with ajax jquery</h5>
    <h6>You pay</h6>
    <div class="col-1">
        <select class="form-select countries" name="fromJs">
            <?php foreach ($countries as $key => $country): ?>
                <option value="<?= $country?>"><?= $country?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="col-2">
        <input type="text" class="form-control" name="amountJs">
    </div>
    <div class="col-offset-4"></div>
    <div class="col-md-3">
        <div class="input-group mb-3 col-3">
            <span class="input-group-text" id="basic-addon3">Exchange rate</span>
            <input type="text" class="form-control " name="rateJs" disabled style="background-color: white">
        </div>
    </div>

    <h6>Go Media will get</h6>

    <div class="col-1">
        <select class="form-select" name="toJs">
            <?php foreach ($countries as $key => $country): ?>
                <option value="<?= $country?>"><?= $country?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="col-2 mb-3 ">
        <input type="text" class="form-control" name="exchangedJs" disabled >
    </div>
</div>
<script>

</script>