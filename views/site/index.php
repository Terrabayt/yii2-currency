<h2>I choose https://exchangeratesapi.io and created 3 examples:</h2>
<ul>
    <li>With package `benmajor/exchange-rates-api` and ajax Jquery</li>
    <li>With package `benmajor/exchange-rates-api`, Form and ActiveForm</li>
    <li>Without package `benmajor/exchange-rates-api`, endpoint and ajax Jquery</li>
</ul>