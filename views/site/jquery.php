<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */


use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = 'With Package';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('/js/jquery.js', ['depends' => 'yii\web\JqueryAsset']);

?>

<div class="row g-3 bg-light">
    <h5>Here used `benmajor/exchange-rates-api` package in controller and it called with ajax using jquery</h5>
    <h6>You pay</h6>
    <div class="col-1">
        <select class="form-select countries" name="from">
            <?php foreach ($countries as $key => $country): ?>
                <option value="<?= $country?>"><?= $country?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="col-2">
        <input type="text" class="form-control" name="amount">
    </div>
    <div class="col-offset-4"></div>
    <div class="col-md-3">
        <div class="input-group mb-3 col-3">
            <span class="input-group-text" id="basic-addon3">Exchange rate</span>
            <input type="text" class="form-control " name="rate" disabled style="background-color: white">
        </div>
    </div>

    <h6>Go Media will get</h6>

    <div class="col-1">
        <select class="form-select" name="to">
            <?php foreach ($countries as $key => $country): ?>
                <option value="<?= $country?>"><?= $country?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="col-2 mb-3 ">
        <input type="text" class="form-control" name="exchanged" disabled >
    </div>
</div>
<script>

</script>