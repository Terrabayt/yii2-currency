<?php

/* @var $this yii\web\View */

/* @var $form yii\bootstrap\ActiveForm */


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->title = 'With Package';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('/js/pjax.js', ['depends' => 'yii\web\JqueryAsset']);

?>
<style>
</style>
<div class="row g-3 bg-light">
    <?php Pjax::begin(['id' => 'currency']); ?>
    <?php
    $form = ActiveForm::begin(['id' => 'currency-form', 'options' => [
        'class' => 'form-horizontal',
        'data-pjax' => true,
    ]]);
    ?>
    <h5>Here used `benmajor/exchange-rates-api` package , Form and ActiveForm. Submiting and reloading every 3 seconds
        with pjax</h5>
    <h6>You pay</h6>

    <div class="row mb-2">
        <div class="input-group">
            <div class="col-md-1">
                <?= $form->field($model, 'from')->dropDownList($countries)->label(false); ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'amount')->textInput()->label(false); ?>
            </div>
        </div>
    </div>
    <div class="row mb-3">
        <div class="input-group">
            <div class="col-md-2">
                <span class="input-group-text" id="basic-addon1">Exchange Rate</span>
            </div>
            <div class="col-md-1">
                <?= $form->field($model, 'rate')->textInput(['disabled' => true, 'aria-describedby' => "basic-addon1", 'style' => "background-color: white"])->label(false); ?>
            </div>
        </div>
    </div>

    <h6>Go Media will get</h6>
    <div class="row mb-2">
        <div class="input-group">
            <div class="col-md-1">
                <?= $form->field($model, 'to')->dropDownList($countries)->label(false); ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'ratedAmount')->textInput()->label(false); ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
</div>
<script>

</script>