$(document).ready(function () {
    // START - Using with Jquery & package
    $('select[name = "from"]').on('change', function () {
        callAjax()
    })

    $('select[name = "to"]').on('change', function () {
        callAjax()
    })

    $('input[name = "amount"]').on('change', function () {
        callAjax()
    })
    var call = setInterval(callAjax, 3000);
    function callAjax() {
        var from = $('select[name = "from"]').val()
        var to = $('select[name = "to"]').val()
        var amount = $('input[name = "amount"]').val()
        if (from && to && amount) {
            $.ajax({
                url: "/site/jquery",
                type: "post",
                data: {
                    'to': to,
                    'amount': amount,
                    'from': from,
                },
                success: function (response) {
                    $('input[name = "rate"]').val(response.rate)
                    $('input[name = "exchanged"]').val(response.ratedAmount)
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        }
    }
    // END - Using with Jquery & package
});