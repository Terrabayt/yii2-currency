$(document).ready(function () {
    // PJAX TRIGGER START

    $(document).on('change','select[name = "CurrencyForm[from]"]', function () {
        trigger()
    })
    $(document).on('change','input[name = "CurrencyForm[amount]"]', function () {
        trigger()
    })
    $(document).on('change','select[name = "CurrencyForm[to]"]', function () {
        trigger()
    })
    var triggerInterval = setInterval(trigger, 3000);

    function trigger(){
        $.pjax.reload({
            container: '#currency',
            type: 'POST',
            data: $('#currency-form').serialize()
        });
    }
    // PJAX TRIGGER END
});