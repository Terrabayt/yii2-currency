$(document).ready(function () {
    // START - Using endpoints and jquery
    $('select[name = "fromJs"]').on('change', function () {
        callEndPoint()
    })

    $('select[name = "toJs"]').on('change', function () {
        callEndPoint()
    })

    $('input[name = "amountJs"]').on('change', function () {
        callEndPoint()
    })
    var call = setInterval(callEndPoint, 3000);
    function callEndPoint() {
        var fromJs = $('select[name = "fromJs"]').val()
        var toJs = $('select[name = "toJs"]').val()
        var amountJs = $('input[name = "amountJs"]').val()
        if (fromJs && toJs && amountJs) {
            $.ajax({
                url: "https://api.exchangeratesapi.io/latest",
                type: "GET",
                data: {
                    'symbols': toJs,
                    'base' : fromJs
                },
                success: function (response) {
                    var rate = response.rates[toJs];
                    $('input[name = "rateJs"]').val(rate)
                    $('input[name = "exchangedJs"]').val(rate * Number(amountJs))
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus, errorThrown);
                }
            });
        }
    }
    // END - Using endpoints and jquery
});